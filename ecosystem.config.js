module.exports = {
    apps: [
        {
            name: 'Rob Wills Design',
            script: '__sapper__/build',
            env: {
                HOST: 'localhost',
                PORT: 3000
            }
        }
    ],
}