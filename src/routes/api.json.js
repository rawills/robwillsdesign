const fetch = require("node-fetch");

const TOKEN = "e45VYSJrly1qBREo61nTeekgNByMX9rsoa1A4bmW_1IFgXHkqLeDDWB9EQHyGhyn";
const API_URL = "https://app.rawills.co.uk/api";


export async function get(req, res) {
  // Get the query
  const query = JSON.parse(JSON.stringify(req.query.query));

  // Fetch the data
  const _req = await fetch(API_URL, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${TOKEN}`
    },
    body: JSON.stringify({ query })
  });

  const json = await _req.json();

  res.end(JSON.stringify(json));
}
