import { writable } from 'svelte/store';

const head = {
  visible: true,
};

const headBg = {
  visible: false,
};

const bgColor = {
  visible: false,
};

const menu = {
  open: false,
};

const modal = {
  open: false,
};


const ui = writable({
  head,
  headBg,
  bgColor,
  menu,
  modal
});

export { ui };
